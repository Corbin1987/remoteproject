# PowerReviews Project
## Enhancements
- ReviewEntity class was created to represent submitted reviews, with corresponding ReviewRepository.
- ReviewController class was created with endpoints to post reviews and to return all reviews for a restaurant.
- Average rating for a restaurant has been added to the endpoint that retrieves restaurant by id.
- Review restrictions:
    - Comments cannot be over 200 characters.
    - Comments cannot contain the words: lit, hella, chill, and/or bro.
    - Ratings must be from 1 to 5.
    - Restaurants must exist in the database.

## Runtime

##### Example CURL requests to run for validation (run in sequence / all code blocks below are bash):

- Get data about a restaurant including the average rating:
```
curl -X GET "http://localhost:8080/restaurant/1" -H "accept: application/json"
```
```
{"id":1,"name":"Chipotle","type":"Tacos","latitude":"41.869612","longitude":"-87.734883","averageRating":null}
```

- Get reviews for a restaurant that does not yet have reviews:

```
curl -X GET "http://localhost:8080/restaurant/1/reviews" -H "accept: application/json" -H "Content-Type: application/json"
```
```
{"Error":"There are no reviews for this restaurant"}
```

- Submit a review:

```
curl -X POST "http://localhost:8080/restaurant/review" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"restaurantId\": 1,\"comments\": \"this is a test review\", \"rating\": 5, \"username\": \"Corbin\"}"
```
```
{"id":1,"restaurantId":1,"rating":5,"comments":"this is a test review","username":"Corbin","time": <CURRENT TIME>}
```

- Submit additional reviews for the same restaurant and get average rating:

```
curl -X POST "http://localhost:8080/restaurant/review" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"restaurantId\": 1,\"comments\": \"this is another test review\", \"rating\": 4, \"username\": \"Corbin\"}"
curl -X POST "http://localhost:8080/restaurant/review" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"restaurantId\": 1,\"comments\": \"this is yet another test review\", \"rating\": 4, \"username\": \"Corbin\"}"
curl -X GET "http://localhost:8080/restaurant/1" -H "accept: application/json"
```
```
{"id":1,"name":"Chipotle","type":"Tacos","latitude":"41.869612","longitude":"-87.734883","averageRating":4}
```

- Return all the reviews for a restaurant:

```
curl -X GET "http://localhost:8080/restaurant/1/reviews" -H "accept: application/json" -H "Content-Type: application/json"
```
```
[{"id":1,"restaurantId":1,"rating":5,"comments":"this is a test review","username":"Corbin","time":<CURRENT TIME>},{"id":2,"restaurantId":1,"rating":4,"comments":"this is another test review","username":"Corbin","time":<CURRENT TIME>},{"id":3,"restaurantId":1,"rating":4,"comments":"this is yet another test review","username":"Corbin","time":<CURRENT TIME>}]
```

- Submit a review with a restricted word:

```
curl -X POST "http://localhost:8080/restaurant/review" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"restaurantId\": 1,\"comments\": \"this is a bro review\", \"rating\": 4, \"username\": \"Corbin\"}"
```
```
[{"Error":"Review comments cannot contain restricted words"}]
```

- Submit a review with an invalid rating:

```
curl -X POST "http://localhost:8080/restaurant/review" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"restaurantId\": 1,\"comments\": \"this is a review with an invalid rating\", \"rating\": 6, \"username\": \"Corbin\"}"
```
```
[{"Error":"Rating must be from 1 to 5"}]
```

- Submit a review with over 200 characters in comments:

```
curl -X POST "http://localhost:8080/restaurant/review" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"restaurantId\": 1,\"comments\": \"This review is too long, and it should not get submitted. It should return an error message when a user attempts to post it because the length of the comments exceeds the amount that is allowed by the application.\", \"rating\": 5, \"username\": \"Corbin\"}"
```
```
[{"Error":"Review comments cannot exceed 200 characters"}]
```

- Submit a review with multiple errors:

```
curl -X POST "http://localhost:8080/restaurant/review" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"restaurantId\": 1,\"comments\": \"This review is too long, and it should not get submitted. It should return an error message when a user attempts to post it because the length of the comments exceeds the amount that is allowed by the application... bro.\", \"rating\": 6, \"username\": \"Corbin\"}"
```
```
[{"Error":"Review comments cannot contain restricted words"},{"Error":"Review comments cannot exceed 200 characters"},{"Error":"Rating must be from 1 to 5"}]
```

## Testing with Swagger
- Open http://localhost:8080/swagger-ui.html in browser when application is running.

## Other notes

- Submitted by: Corbin Via
- Branch "corbin-via-coding-test" was used for changes.
- Branch "default" represents the original master branch before any merges.