package com.powerreviews.project.controller;

import com.powerreviews.project.persistence.RestaurantEntity;
import com.powerreviews.project.persistence.RestaurantRepository;
import com.powerreviews.project.persistence.ReviewEntity;
import com.powerreviews.project.persistence.ReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
public class ReviewController {
    @Autowired
    private final ReviewRepository reviewRepository;
    @Autowired
    private final RestaurantRepository restaurantRepository;
    private Timestamp timestamp;

    public ReviewController(ReviewRepository reviewRepository, RestaurantRepository restaurantRepository) {
        this.reviewRepository = reviewRepository;
        this.restaurantRepository = restaurantRepository;
    }

    @ResponseBody
    @RequestMapping(value = "/restaurant/review", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> postReview(@RequestBody ReviewEntity review) {
        Optional<RestaurantEntity> restaurant = restaurantRepository.findById(review.getRestaurantId());
        if (restaurant.equals(Optional.empty())) {
            return new ResponseEntity<>(createErrorMap("Your review cannot be submitted because the restaurant specified was not found"),
                    HttpStatus.BAD_REQUEST);
        }
        List<Map<String, String>> validation = validateReview(review);
        if (!validation.isEmpty()) {
            return new ResponseEntity<>(validation, HttpStatus.BAD_REQUEST);
        }
        review.setTime(new java.util.Date());
        reviewRepository.save(review);
        return new ResponseEntity<>(review, new HttpHeaders(), HttpStatus.OK);
    }

    @ResponseBody
    @RequestMapping(value = "/restaurant/{id}/reviews", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getReviews(@PathVariable Integer id) {
        Optional<RestaurantEntity> restaurant = restaurantRepository.findById(id);
        if (restaurant.equals(Optional.empty())) {
            return new ResponseEntity<>(createErrorMap("The restaurant that you specified was not found"),
                    HttpStatus.BAD_REQUEST);
        }
        List<ReviewEntity> reviews = reviewRepository.findByRestaurantId(id);
        if (reviews.isEmpty()) {
            return new ResponseEntity<>(createErrorMap("There are no reviews for this restaurant"),
                    HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(reviews, new HttpHeaders(), HttpStatus.OK);
    }

    private Map<String, String> createErrorMap(String error) {
        Map<String, String> errorMessage = new HashMap<>();
        errorMessage.put("Error", error);
        return errorMessage;
    }

    private List<Map<String, String>> validateReview(ReviewEntity review) {
        List<Map<String, String>> errors = new ArrayList<>();
        String pattern = "[\\w|\\W]*lit[\\w|\\W]*|[\\w|\\W]*hella[\\w|\\W]*|[\\w|\\W]*chill[\\w|\\W]*|[\\w|\\W]*bro[\\w|\\W]*";
        if (Pattern.matches(pattern, review.getComments())) {
            errors.add(createErrorMap("Review comments cannot contain restricted words"));
        }
        if (review.getComments().length() > 200) {
            errors.add(createErrorMap("Review comments cannot exceed 200 characters"));
        }
        if (review.getRating() > 5 || review.getRating() < 1) {
            errors.add(createErrorMap("Rating must be from 1 to 5"));
        }
        return errors;
    }
}
